unit DMW.Handler.Home;

interface uses
  {$region 'Includes'}
  web_config,
  DMW.Common,
  System.Classes,
  System.SysUtils,
  System.IOUtils;
  {$endregion}

function HomeGetPage: TStrings;

implementation

function HomeGetPage: TStrings;
begin
  Result := ReadyContent('home','mylayout');
end;

end.
