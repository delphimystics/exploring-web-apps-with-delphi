unit DMW.Handler.About;

interface uses
  {$region 'Includes'}
  web_config,
  DMW.Common,
  System.Classes,
  System.SysUtils,
  System.IOUtils;
  {$endregion}

function AboutGetPage: TStrings;

implementation

function AboutGetPage: TStrings;
begin
  Result := ReadyContent('about','mylayout');
end;

end.
