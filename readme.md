# Series: Demystifying Development #
**Episode: Exploring Web Apps with Delphi**  
**Release Date: 3/16/2020** (mm/dd)  
**Filmed: 3/11/2020** (mm/dd)

## Description (Episode Overview & Disclaimer)##
The following video was requested by a large number of users.  
In this multi-part of episode we will be exploring what it is like for a new user to try creating a web application for the first time with Delphi. There was minimal editing done to these, this was by design. We wanted to capture the unique experience of a developer creating their first web app with Delphi without polish. There will be a properly planned series posted at a later date. Please listen to the first episode to better understand why we recorded this in this way.  
Episode: Exploring Web Apps with Delphi

*List of Episode Parts*    
- Part 1 - The Goals and Intentions - 10m: [https://youtu.be/hxTujKpFtQo](https://youtu.be/hxTujKpFtQo "Part 1 - The Goals and Intentions - 10m")  
- Part 2 - The Setup - 17m: [https://youtu.be/MIKG2k7nVEA](https://youtu.be/MIKG2k7nVEA "Part 2 - The Setup - 17m")  
- Part 3 - The Basics - 15m: [https://youtu.be/cRSkNrY8dtM](https://youtu.be/cRSkNrY8dtM "Part 3 - The Basics - 15m")  
- Part 4 - Templating - 14m: [https://youtu.be/MpjsBqUK6K8](https://youtu.be/MpjsBqUK6K8 "Part 4 - Templating - 14m")  
- Part 5 - Using Bootstrap - 24m: [https://youtu.be/a5FljfYs0Mc](https://youtu.be/a5FljfYs0Mc "Part 5 - Using Bootstrap - 24m")  
- Part 6 - Implementing Layouts - 38m: [https://youtu.be/70ocUAm2DYM](https://youtu.be/70ocUAm2DYM "Part 6 - Implementing Layouts - 38m")  
- Part 7 - Using Routing - 10m: [https://youtu.be/abT2LcWKeq0](https://youtu.be/abT2LcWKeq0 "Part 7 - Using Routing - 10m")  

**Hosts:** Zach Briggs

## Episode Links##  
*Delphi Mystics - Demystifying Development - Exploring Web Apps with Delphi*  
- Part 1 - The Goals and Intentions - 10m: [https://youtu.be/hxTujKpFtQo](https://youtu.be/hxTujKpFtQo "Part 1 - The Goals and Intentions - 10m")  
- Part 2 - The Setup - 17m: [https://youtu.be/MIKG2k7nVEA](https://youtu.be/MIKG2k7nVEA "Part 2 - The Setup - 17m")  
- Part 3 - The Basics - 15m: [https://youtu.be/cRSkNrY8dtM](https://youtu.be/cRSkNrY8dtM "Part 3 - The Basics - 15m")  
- Part 4 - Templating - 14m: [https://youtu.be/MpjsBqUK6K8](https://youtu.be/MpjsBqUK6K8 "Part 4 - Templating - 14m")  
- Part 5 - Using Bootstrap - 24m: [https://youtu.be/a5FljfYs0Mc](https://youtu.be/a5FljfYs0Mc "Part 5 - Using Bootstrap - 24m")  
- Part 6 - Implementing Layouts - 38m: [https://youtu.be/70ocUAm2DYM](https://youtu.be/70ocUAm2DYM "Part 6 - Implementing Layouts - 38m")  
- Part 7 - Using Routing - 10m: [https://youtu.be/abT2LcWKeq0](https://youtu.be/abT2LcWKeq0 "Part 7 - Using Routing - 10m")  
- Source Code: [https://bitbucket.org/delphimystics/exploring-web-apps-with-delphi/src/master/](https://bitbucket.org/delphimystics/exploring-web-apps-with-delphi/src/master/ "Source Code")  

*Host Links*  
- Jim McKeeth's Blog: [http://delphi.org](http://delphi.org "Jim McKeeth's Blog")  
- Delphi Worlds: [https://www.delphiworlds.com/](https://www.delphiworlds.com/ "Delphi Worlds Website")  
- Dave Nottage's Blog: [https://www.delphiworlds.com/blog/](https://www.delphiworlds.com/blog/ "Dave Nottage's Blog")  
- Rapid Kinetics: [https://rapidkinetics.com](https://rapidkinetics.com "Rapid Kinetics LLC Website")  
- Zach Briggs' Portfolio: [https://zachbriggs.name](https://zachbriggs.name "Zach Briggs' Portfolio")  

*Delphi Mystics*  
- Enclave: [https://www.delphimystics.com](https://delphimystics.com "Delphi Mystics Enclave Site")  
- Show: [https://www.delphimystics.com/show/delphi-discussions/](https://www.delphimystics.com/show/delphi-discussions/ "Delphi Discussions Show Site")   
- Shop: [https://www.delphimystics.com/shop](https://www.delphimystics.com/shop "Delphi Mystics Shop")  
- Facebook: [https://www.facebook.com/delphimystics](https://www.facebook.com/delphimystics "Delphi Mystics Facebook")  
- GitHub: [https://github.com/Delphi-Mystics](https://github.com/Delphi-Mystics "Delphi Mystics GitHub")  
- Instagram: [https://www.instagram.com/delphimystics/](https://www.instagram.com/delphimystics/ "Delphi Mystics Instagram")  
- Patreon: [https://www.patreon.com/delphimystics](https://www.patreon.com/delphimystics "Delphi Mystics Patreon")  
- Twitch: [https://www.twitch.tv/delphimystics](https://www.twitch.tv/delphimystics "Delphi Mystics Twitch")  
- Twitter: [https://twitter.com/DelphiMystics](https://twitter.com/DelphiMystics "Delphi Mystics Twitter")  
- YouTube: [https://www.youtube.com/channel/UCNzPZhBjENZcK8TtM20sxZg](https://www.youtube.com/channel/UCNzPZhBjENZcK8TtM20sxZg "Delphi Mystics YouTube Channel")  

*Join Delphi Worlds Slack Community*  
- [https://slack.delphiworlds.com/](https://slack.delphiworlds.com/ "Delphi Worlds Slack Community")  