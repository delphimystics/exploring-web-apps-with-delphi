library mod_dmwa24a;

uses
  {$IFDEF MSWINDOWS}
  Winapi.ActiveX,
  System.Win.ComObj,
  {$ENDIF }
  Web.WebBroker,
  Web.ApacheApp,
  Web.HTTPD24Impl,
  webmoduleMaster in 'webmoduleMaster.pas' {WebModule1: TWebModule},
  web_config in 'includes\web_config.pas',
  DMW.Handler.About in 'handlers\DMW.Handler.About.pas',
  DMW.Common in 'lib\DMW.Common.pas',
  DMW.Handler.Home in 'handlers\DMW.Handler.Home.pas';

{$R *.res}

// httpd.conf entries:
//
(*
 LoadModule dmwa24a_module modules/mod_dmwa24a.dll

 <Location /xyz>
    SetHandler mod_dmwa24a-handler
 </Location>
*)
//
// These entries assume that the output directory for this project is the apache/modules directory.
//
// httpd.conf entries should be different if the project is changed in these ways:
//   1. The TApacheModuleData variable name is changed.
//   2. The project is renamed.
//   3. The output directory is not the apache/modules directory.
//   4. The dynamic library extension depends on a platform. Use .dll on Windows and .so on Linux.
//

// Declare exported variable so that Apache can access this module.
var
  GModuleData: TApacheModuleData;
exports
  GModuleData name 'dmwa24a_module';

begin
{$IFDEF MSWINDOWS}
  CoInitFlags := COINIT_MULTITHREADED;
{$ENDIF}
  Web.ApacheApp.InitApplication(@GModuleData);
  Application.Initialize;
  Application.WebModuleClass := WebModuleClass;
  Application.Run;
end.
