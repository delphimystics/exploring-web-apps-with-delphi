unit DMW.Common;

interface uses
  {$region 'Includes'}
  web_config,
  System.Classes,
  System.SysUtils,
  System.IOUtils;
  {$endregion}

procedure TagSwap(ATag: String; AValue: String; var ASource: TStrings);
function ReadyContent(ATemplateName: String; ALayoutName: String): TStrings;

implementation

function ReadyContent(ATemplateName: String; ALayoutName: String): TStrings;
var
  slLayout: TStrings;
  slHeader: TStrings;
  slFooter: TStrings;
  slNavbar: TStrings;
  slContent: TStrings;
  sLayout: String;
  sHeader: String;
  sNavbar: String;
  sFooter: String;
begin
  slLayout := TStringList.Create;
  slHeader := TStringList.Create;
  slFooter := TStringList.Create;
  slNavbar := TStringList.Create;
  slContent := TStringList.Create;

  sLayout := g_TemplatesPath + PathDelim + ALayoutName + '.html';
  if FileExists(sLayout) = false then sLayout := '';

  sHeader := g_TemplatesPath + PathDelim + ALayoutName + '-header.html';
  if FileExists(sLayout) = false then sHeader := '';

  sNavbar := g_TemplatesPath + PathDelim + ALayoutName + '-navbar.html';
  if FileExists(sLayout) = false then sNavbar := '';

  sFooter := g_TemplatesPath + PathDelim + ALayoutName + '-footer.html';
  if FileExists(sLayout) = false then sFooter := '';


  if ((sLayout <> '') and (sHeader <> '') and (sNavBar <> '') and (sFooter <> '')) then
    begin
      slLayout.LoadFromFile(sLayout);
      slHeader.LoadFromFile(sHeader);
      slNavBar.LoadFromFile(sNavBar);
      slFooter.LoadFromFile(sFooter);

      TagSwap('AppName',g_AppName,slHeader);
      TagSwap('AppOwner',g_AppOwner,slHeader);
      TagSwap('AppCopyright',g_AppCopyright,slHeader);
      TagSwap('AppVer',g_AppVer,slHeader);
      TagSwap('WebPath',g_WebPath,slHeader);


      TagSwap('AppName',g_AppName,slNavBar);
      TagSwap('AppOwner',g_AppOwner,slNavBar);
      TagSwap('AppCopyright',g_AppCopyright,slNavBar);
      TagSwap('AppVer',g_AppVer,slNavBar);
      TagSwap('WebPath',g_WebPath,slNavBar);

      TagSwap('AppName',g_AppName,slFooter);
      TagSwap('AppOwner',g_AppOwner,slFooter);
      TagSwap('AppCopyright',g_AppCopyright,slFooter);
      TagSwap('AppVer',g_AppVer,slFooter);
      TagSwap('WebPath',g_WebPath,slFooter);

      TagSwap('Header',slHeader.Text,slLayout);
      TagSwap('NavBar',slNavbar.Text,slLayout);
      TagSwap('Footer',slFooter.Text,slLayout);

    end
  else
    begin
      Result.LoadFromFile(g_ErrorsPath + PathDelim + '404.html');
      exit;
    end;

  if FileExists(g_TemplatesPath + PathDelim + ATemplateName + '.html') then
    begin
      slContent.LoadFromFile(g_TemplatesPath + PathDelim + ATemplateName + '.html');
      TagSwap('AppName',g_AppName,slContent);
      TagSwap('AppOwner',g_AppOwner,slContent);
      TagSwap('AppCopyright',g_AppCopyright,slContent);
      TagSwap('AppVer',g_AppVer,slContent);
      TagSwap('WebPath',g_WebPath,slContent);

      TagSwap('Content',slContent.Text,slLayout);

      Result := slLayout;
    end
  else
    Result.LoadFromFile(g_ErrorsPath + PathDelim + '404-template.html');
end;

procedure TagSwap(ATag: String; AValue: String; var ASource: TStrings);
var
  thisLine: String;
  slResult: TStrings;
begin
  slResult := TStringList.Create;
  for thisLine in ASource do
    begin
      slResult.Add(thisLine.Replace('^^' + ATag + '^^', AValue));
    end;
  ASource := slResult;
end;

end.
